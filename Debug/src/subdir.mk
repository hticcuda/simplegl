################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CU_SRCS += \
../src/simpleGL.cu 

OBJS += \
./src/simpleGL.o 

CU_DEPS += \
./src/simpleGL.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cu
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-8.0/bin/nvcc -I"/usr/local/cuda-8.0/samples/2_Graphics" -I"/usr/local/cuda-8.0/samples/common/inc" -I"/home/htic/Desktop/GPU/simpleGL" -G -g -O0 -gencode arch=compute_20,code=sm_20 -m64 -odir "src" -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-8.0/bin/nvcc -I"/usr/local/cuda-8.0/samples/2_Graphics" -I"/usr/local/cuda-8.0/samples/common/inc" -I"/home/htic/Desktop/GPU/simpleGL" -G -g -O0 --compile --relocatable-device-code=false -gencode arch=compute_20,code=compute_20 -gencode arch=compute_20,code=sm_20 -m64  -x cu -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


